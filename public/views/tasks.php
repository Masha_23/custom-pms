<?php
require_once dirname(dirname(__DIR__)).'/com/fileLoader.php';
$boardId = $_GET['boardID'];
$user = TBHelper::getUserByToken($_SESSION['token']);
$board = TBBoards::getBoardById($boardId);

if (!in_array($user['email'], $board['assignedUsers'])) {
    header("location:forbidden.html");
}


$statuses = $board['statuses'];
$tasks = TBTasks::getSavedTaskData($boardId);
?>

<head>
    <?php require_once 'header.php' ?>
</head>
<body>

    <!-- tasks  board  -->
    <div class="tb-board-wrapper">
        <div class="tb-top-container">
            <h1>Board : <?php echo $board['name']?></h1>
        </div>
        <div class="tb-bottom-container">
            <?php foreach ($statuses as $key => $value) : ?>
                <div class="col-md-<?php echo (int)(12 / count($statuses))?> tb-board-col">
                    <div class="tb-board-col-name"><?php echo $value; ?></div>
                    <div class="tb-tasks">
                        <!-- task card   -->
                        <?php foreach ($tasks as $task) :?>
                            <?php if($task['status'] == $value) :?>
                                <div class="card tb-task-card" id="tb-card-<?php echo $task['ID'];?>" data-taskId="<?php echo $task['ID'];?>">
                                    <div class="card-body">
                                        <h5 class="card-title"> <?php echo $task['title']; ?> </h5>
                                        <p class="card-text"> <?php echo $task['description']; ?> </p>
                                        <div class="alert alert-primary" role="alert">
                                            assigned to <b><?php echo $task['email']; ?></b>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <button type="submit" class="tb-add-btn tb-add-task-btn" data-attr-action="add-task" data-toggle="modal" data-target="#createTask"></button>
    </div>

    <!--  create task  -->
    <div class="modal fade" id="createTask" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog tb-add-task-modal" role="document">
            <div class="modal-content tb-add-modal-content">
                <div class="tb-add-task-section flex">
                    <div class="tb-add-task-name input-group">
                        <input id="taskCrateName" type="text" class="form-control" placeholder="Name" required>
                    </div>
                    <div class="tb-add-task-assign input-group">
                        <select name="" id="taskCrateAssign" class="form-control">
                            <option disabled selected>Assign To</option>
                            <?php foreach($board['assignedUsers'] as $userid => $email) :?>
                                <option data-userId="<?php echo $userid; ?>"><?php echo $email; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <input id="taskCrateStatus" type="hidden" class="form-control" value="<?php echo array_key_first($statuses) ;?>">
                </div>
                <div class="tb-add-task-section">
                    <textarea id="taskCrateDesc" class="form-control" aria-label="With textarea" placeholder="Description" rows="10"></textarea>
                </div>
                <div class="tb-crate-button-container">
                    <button id="taskCrateSave" type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>

    <!--  show task in modal -->
    <div class="modal fade" id="showTask" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog tb-show-task-modal" role="document">
            <div class="modal-content tb-show-modal-content">
                <div class="tb-show-task-section flex">
                    <div class="tb-show-task-name input-group">
                        <p id="tb-show-task-title">title</p>
                    </div>
                    <div class="tb-show-task-assign input-group">
                        <p id="tb-show-task-assign">assign to</p>
                    </div>
                </div>
                <div class="tb-show-task-section">
                    <p id="tb-show-task-description">desc</p>
                </div>
                <div class="tb-show-task-section">
                    <select name="" id="tb-show-task-status" class="form-control">
                        <?php foreach($board['statuses'] as $statusId => $status) :?>
                            <option data-userId="<?php echo $statusId; ?>"><?php echo $status; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="tb-show-task-section">
                    Archive
                    <label class="switch" id="switch">
                        <input type="checkbox" id="tb-archive">
                        <span class="slider round"></span>
                    </label>
                </div>
                <div class="tb-crate-button-container">
                    <button id="taskUpadte" type="button" class="btn btn-primary">Update</button>
                </div>

                <input type="hidden" id="tb-update-task" value="">
            </div>
        </div>
    </div>

    <?php require_once 'footer.php' ?>
    <script type="text/javascript" src="../js/tasks.js"></script>
</body>



