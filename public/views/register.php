<?php
session_start();

if (isset($_SESSION['token']) && $_SESSION['token']) {
    header("location: dashboard.php");
}

?>
<head>
    <?php require_once 'header.php' ?>
</head>
<body>
    <div class="row tb-main-wrapper">
        <form>
            <div class="form-group">
                <label for="tb-email-field">Email address</label>
                <input type="email" class="form-control" aria-describedby="emailHelp" placeholder="Enter email" id="tb-email-field" name="tb-mail">
                <small class="form-text text-danger tb-required-field-message tb-hide" data-attr-field="tb-mail">This field
                    is required</small>
            </div>
            <div class="form-group">
                <label for="tb-name-field">Name</label>
                <input type="text" class="form-control" placeholder="Enter name" id="tb-name-field" name="tb-name">
                <small class="form-text text-danger tb-required-field-message tb-hide" data-attr-field="tb-name">This field
                    is required</small>
            </div>
            <div class="form-group">
                <label for="tb-password-field">Password</label>
                <input type="password" class="form-control" placeholder="Password" id="tb-password-field" name="tb-password">
                <small class="form-text text-danger tb-required-field-message tb-hide" data-attr-field="tb-password">This
                    field is required</small>
            </div>

            <button type="submit" class="btn btn-primary tb-submit" data-attr-action="registration">Submit</button>
        </form>
    </div>

    <?php require_once 'footer.php' ?>
    <script type="text/javascript" src="../js/script.js"></script>
</body>
