<?php
session_start();
//unset($_SESSION['token']);

if (!isset($_SESSION['token']) && !$_SESSION['token']) {
    header("location: login.php");
}
require_once dirname(dirname(__DIR__)).'/com/fileLoader.php';
$user = TBHelper::getUserByToken($_SESSION['token']);
$boards = TBBoards::getUserBoards($user['ID']);
?>

<head>
    <?php require_once 'header.php' ?>
</head>
<body>
    <!-- boards  -->
    <div class="tb-board-wrapper">
        <div class="tb-top-container">
            <button type="button" class="btn btn-danger" id="tb-logout-btn" style="float: right">LogOut</button>
            <h1>Hi <?php echo $user['name']?></h1>
        </div>
        <div class="tb-bottom-container">
            <!--   boards list         -->
            <?php foreach ($boards as $board) :?>
                <?php
                    $boardId = $board['ID'];
                    $redirectUrl =  "location.href='tasks.php?boardID={$boardId}'";
                ?>
                <div class="card tb-board-card" id="tb-board-card-<?php echo $board['ID'];?>" data-taskId="<?php echo $board['ID'];?>">
                    <div class="card-body">
                        <h5 class="card-title text-primary"> <?php echo $board['name']; ?> </h5>
                        <button  onclick=<?php echo $redirectUrl; ?> id="openBoard" type="button" class="btn btn-primary">Open</button>
                    </div>
                </div>
            <?php endforeach; ?>
       </div>
        <button type="submit" class="tb-add-btn tb-add-board-btn" data-attr-action="add-board" data-toggle="modal" data-target="#createBoard"></button>
    </div>

    <!--  create board  -->
    <div class="modal fade" id="createBoard" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog tb-add-board-modal" role="document">
            <div class="modal-content tb-add-modal-content">
                <div class="tb-add-board-section flex">
                    <div class="tb-add-board-name input-group">
                        <input id="boardCrateName" type="text" class="form-control" placeholder="Name" required>
                    </div>
                </div>
                <div class="tb-add-board-assign">
                    <input id="boardCrateAssign" type="text" class="form-control" placeholder="Invite to" data-creator="<?php echo $user['email'];?>" required>
                </div>
                <div class="tb-add-board-section">
                    <input id="boardCrateStatuses" type="text" class="form-control" placeholder="Statuses" required>
                </div>
                <div class="tb-crate-button-container">
                    <button id="boardCreateSave" type="button" class="btn btn-primary" >Save</button>
                </div>
            </div>
        </div>
    </div>


    <?php require_once 'footer.php' ?>
    <script type="text/javascript" src="../js/boards.js"></script>
</body>
