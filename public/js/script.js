function submitButtobClick() {
    var hasError = false;
    var requiredFileds = jQuery('.tb-required-field-message');
    var length = requiredFileds.length;
    var configData = {};

    for (var index = 0; index < length; index++) {
        var field = requiredFileds[index];
        var fieldName = jQuery(field).data('attr-field');
        var fieldValue = jQuery('input[name='+fieldName+']').val();

        if (!fieldValue) {
            jQuery(field).removeClass('tb-hide');
            hasError = true;
        }
        configData[fieldName] = fieldValue;
    }
    if (!hasError) {
        return configData;
    }
}

function afterSubmit(action, formData={})
{
    var ajaxData = {
        action: action,
        data: {
            submittedData: formData,
            beforeSend: function() {
                jQuery('.tb-submit').attr('disabled', true);
                jQuery('.tb-required-field-message').addClass('tb-hide');
            }
        }
    };
    var ajaxurl = window.location.origin+'/tasksBoard/com/ajax.php';
    jQuery.post(ajaxurl, ajaxData, function(response) {
        if (response) {
            location.replace(window.location.origin+'/tasksBoard/');
        }
        else {
            location.reload();
        }
    });
}


jQuery('.tb-submit').click(function(e){
    e.preventDefault();
    var action = jQuery(this).data('attr-action');
    if (action == 'logout') {
        afterSubmit(action);
    }
    var formData = submitButtobClick();

    if (formData !== undefined) {
        afterSubmit(action, formData);
    }
});



