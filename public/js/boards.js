window.onload = () => {
    /* create board */
    jQuery('#boardCreateSave').on('click', function () {
        var name = jQuery('#boardCrateName').val();
        var invite = jQuery('#boardCrateAssign').val();
        var user = jQuery('#boardCrateAssign').attr('data-creator');
        invite = user + ',' + invite;
        var statuses = jQuery('#boardCrateStatuses').val();

        if (!name.length || !invite.length || !statuses.length) {
            return '';
        }
        var ajaxData = {
            action: "create-board",
            data: {
                submittedData: {name, invite, statuses}
            }
        };

        var ajaxurl = window.location.origin+'/tasksBoard/com/ajax.php';
        jQuery.post(ajaxurl, ajaxData, function (response) {
            jQuery('#createBoard').modal('hide');
            location.reload();
        });
    });

    jQuery('#tb-logout-btn').click(function () {
        var ajaxData = {
            action: 'logout',
        };

        var ajaxurl = window.location.origin+'/tasksBoard/com/ajax.php';
        jQuery.post(ajaxurl, ajaxData, function (response) {
            location.reload();
        });
    });
};

