window.onload = () => {
    /* create task */
    jQuery('#taskCrateSave').on('click', function () {
        var name = jQuery('#taskCrateName').val();
        var desc = jQuery('#taskCrateDesc').val();
        var assign = jQuery('#taskCrateAssign').val();
        var status = jQuery('#taskCrateStatus').val();
        var query= window.location.search;
        var boardId = query[query.length-1];

        if (!name.length || !assign.length) {
            return '';
        }
        var ajaxData = {
            action: 'create-task',
            data: {
                submittedData: {name, desc, assign, status, boardId}
            }
        };
        var ajaxurl = window.location.origin+'/tasksBoard/com/ajax.php';
        jQuery.post(ajaxurl, ajaxData, function (response) {
            if (response) {
                jQuery('#createTask').modal('hide');
                location.reload();
            }
        });
    });

    /* show task */
    jQuery('.tb-task-card').click(function () {
        var taskId = jQuery(this).attr('data-taskId');
        var ajaxData = {
            action: 'show-task',
            data: {
                submittedData: {taskId}
            }
        };
        var ajaxurl = window.location.origin+'/tasksBoard/com/ajax.php';
        jQuery.post(ajaxurl, ajaxData, function (response) {
            if (response) {
                response = JSON.parse(response)[0];
                jQuery('#tb-show-task-title').text(response.title);
                jQuery('#tb-show-task-assign').text(response.email);
                jQuery('#tb-show-task-description').text(response.description);
                jQuery('#tb-show-task-status').val(response.status).find("option[value=" + response.status +"]").attr('selected', true);
                jQuery('#tb-update-task').val(response.ID);
                jQuery('#showTask').modal('show');
            }
        });
    });

    /* Update Task*/
    jQuery('#taskUpadte').click(function () {
        var status = jQuery('#tb-show-task-status').children("option:selected").attr('data-userId');
        var taskId = jQuery('#tb-update-task').val();
        var archiveStatus = jQuery('#tb-archive').is(':checked') ? 1 : 0;
        console.log(archiveStatus);
        var ajaxData = {
            action: 'update-task',
            data: {
                submittedData: {taskId, status, archiveStatus}
            }
        };
        var ajaxurl = window.location.origin+'/tasksBoard/com/ajax.php';
        jQuery.post(ajaxurl, ajaxData, function (response) {
            if (response) {
                jQuery('#updateTask').modal('hide');
                location.reload();
            }
        });

    });
};

