<?php

if (!defined('TB_PROJET_FILE_NAME')) {
    define('TB_PROJET_FILE_NAME', dirname(__FILE__).'/');
}

require_once TB_PROJET_FILE_NAME.'com/fileLoader.php';
header('location: public/views/dashboard.php');