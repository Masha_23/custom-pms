<?php

class TBConfig
{
    public static function addDefine($name, $value)
    {
        if (!defined($name)) {
            define($name, $value);
        }
    }

    public static function init()
    {
        if (!defined('TB_PROJET_FILE_NAME')) {
            define('TB_PROJET_FILE_NAME', dirname(dirname(__FILE__)).'/');
        }

        self::addDefine('TB_PROJET_COM_PATH', TB_PROJET_FILE_NAME.'com/');
        self::addDefine('TB_PROJET_PUBLIC_PATH', TB_PROJET_FILE_NAME.'public/');
        self::addDefine('TB_PROJET_VIEWS_PATH', TB_PROJET_PUBLIC_PATH.'views/');
        self::addDefine('TB_PROJET_DB_PATH', TB_PROJET_COM_PATH.'db/');
        self::addDefine('TB_DATABASE_NAME', 'taskBoard');
    }
}

TBConfig::init();