<?php
require_once 'db/dbInit.php';

class TBTasks {
    private $title;
    private $desc;
    private $assign;
    private $boardId;
    private $status;
    protected static $instance = null;

    private $tasks = [];

    public function __construct($data = array())
    {
        $this->title = $data['name'];
        $this->desc = $data['desc'];
        $this->assign = $data['assign'];
        $this->boardId = $data['boardId'];
        $this->status = $data['status'];
    }

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function saveTaskData() {
        global $tbdb;

        $user = TBHelper::emailExist($this->assign);
        $this->assign = $user['ID'];

        $sql = "
            INSERT INTO Tasks 
                (title, board_id, status_id, description, assigned_to)
            Values (?, ?, ?, ?, ?)
        ";

        $stmt = $tbdb->prepare($sql);
        $success = $stmt->execute([$this->title, $this->boardId, $this->status, $this->desc, $this->assign]);

        return $success;
    }

    public static function getSavedTaskData($currentId = 0, $byBoardId = true, $archive = 0) {
        global  $tbdb;

        $condition = 'WHERE tasks.board_id = ?';
        if (!$byBoardId) {
           $condition = 'WHERE tasks.ID = ?';
        }


        $stmt = $tbdb->prepare('
            SELECT tasks.ID, tasks.title, tasks.description, statuses.status, users.email
            FROM tasks
            INNER JOIN statuses
                on statuses.ID = tasks.status_id
            INNER JOIN users
                on users.ID = tasks.assigned_to and tasks.archive_status = 0 '.$condition
        );
        $stmt->execute([$currentId]);
        $task = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $task;
    }

    public static function upsateTaskData($data) {
        global $tbdb;

        $stmt = $tbdb->prepare('UPDATE tasks SET status_id= ?, archive_status= ? WHERE ID = ?');
        $success = $stmt->execute([$data['status'], $data['archiveStatus'], $data['taskId']]);

        return $success;
    }
}



