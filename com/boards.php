<?php

require_once 'db/dbInit.php';
require_once 'helper.php';

class TBBoards {
    private $name;
    private $invitations;
    private $statuses;
    protected static $instance = null;
    private $boardId;

    public function __construct($data = array())
    {
        $this->name = $data['name'];
        $this->invitations = $data['invite'];
        $this->statuses = $data['statuses'];
    }

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function saveBoardData()
    {
        $this->invitations = TBHelper::stringToArray($this->invitations);
        $this->statuses = TBHelper::stringToArray($this->statuses);

        $this->boardId = $this->saveIntoBoardTable();
        $this->saveIntoUserToBoardTable();
        $this->saveIntoStatusesTable();

        return '';
    }

    public function saveIntoStatusesTable() {
        global $tbdb;

        foreach ($this->statuses  as $status) {
            $query = 'INSERT INTO Statuses(status, board_id) Values(?, ?)';
            $stmt = $tbdb->prepare($query);
            $success = $stmt->execute([$status, $this->boardId]);
        }
    }

    public function saveIntoUserToBoardTable() {
        global $tbdb;

        foreach ($this->invitations as $email) {
            $user = TBHelper::emailExist($email);
            if ($user) {
                $query = 'INSERT INTO UserToBoard(user_id, board_id) Values(?, ?)';
                $stmt = $tbdb->prepare($query);
                $success = $stmt->execute([$user['ID'], $this->boardId]);
            }
        }
    }

    public function saveIntoBoardTable() {
        global $tbdb;

        $query = 'INSERT INTO Boards(name) Values(?)';
        $stmt = $tbdb->prepare($query);
        $success = $stmt->execute([$this->name]);
        if ($success) {
            $stmt = $tbdb->prepare('SELECT MAX(ID) FROM Boards');
            $stmt->execute();
            $id = $stmt->fetch(\PDO::FETCH_ASSOC)['MAX(ID)'];
        }

        if ($id) {
            return $id;
        }

        return false;
    }

    public static function getUserBoards($currentId, $byUserId = true) {
        global  $tbdb;

        if ($currentId) {
            $condition = 'WHERE usertoboard.user_id = ?';
            $sql = '
                SELECT boards.ID, boards.name, usertoboard.user_id FROM boards 
                INNER JOIN usertoboard ON boards.ID = usertoboard.board_id ';
            if (!$byUserId) {
                $condition = 'WHERE usertoboard.board_id = ?';
            }
            $stmt = $tbdb->prepare($sql.$condition);
            $stmt->execute([$currentId]);
            $boards = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }

        return $boards;
    }

    public static function getBoardById($boardId) {
        global $tbdb;

        $board = self::getUserBoards($boardId, false);

        $stmt = $tbdb->prepare('SELECT statuses.status, statuses.ID as statusId FROM statuses WHERE board_id = ?');
        $stmt->execute([$boardId]);
        $statuses = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $assignedUsers = [];
        foreach ($board as $value) {
            $userId = $value['user_id'];
            $stmt = $tbdb->prepare('SELECT users.email FROM users WHERE ID = ?');
            $stmt->execute([$userId]);
            $email = $stmt->fetch(PDO::FETCH_NUM)[0];
            $assignedUsers[$userId] = $email;
        }
        $board = call_user_func_array('array_merge', $board);
        $board['assignedUsers'] = $assignedUsers;
        foreach ($statuses as $status) {
            $board['statuses'][$status['statusId']] = $status['status'];
        }


        return $board;
    }
}








