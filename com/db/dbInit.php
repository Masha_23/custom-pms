<?php
require_once dirname(__DIR__).'/config/config.php';

$defaultTablesData = array(
    'Users' => '`ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `name` varchar(128),
                    `email` varchar(128),
					`password` varchar(128),
					`token` varchar(128),
					PRIMARY KEY (ID)',

    'Boards' => '`ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `name` varchar(128),     			
					 PRIMARY KEY (ID)',

    'UserToBoard' => '`user_id` int(10) UNSIGNED,
                      `board_id` int(10) UNSIGNED,
                      FOREIGN KEY (user_id) REFERENCES Users(ID),
                      FOREIGN KEY (board_id) REFERENCES Boards(ID)',

    'Statuses' => '`ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `status` varchar(128),
                    `board_id` int(10) UNSIGNED,	
					 PRIMARY KEY (ID),
					 FOREIGN KEY (board_id) REFERENCES Boards(ID)',

    'Tasks' => '`ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `title` varchar(128),
                    `board_id` int(10) UNSIGNED,
					`status_id` int(10) UNSIGNED,
					`description` varchar(128),
					`assigned_to` int(10) UNSIGNED,
					`archive_status` int(10) DEFAULT 0,
					 PRIMARY KEY (ID),
					 FOREIGN KEY (assigned_to) REFERENCES Users(ID),
					 FOREIGN KEY (status_id) REFERENCES Statuses(ID),
					 FOREIGN KEY (board_id) REFERENCES Boards(ID)',
);

class initDb
{
    private $dbusername = 'root';
    private $dbpassword = '';
    private $host = 'mysql:host=localhost';
    private $dbname = TB_DATABASE_NAME;
    public static $tbdb;
    protected static $instance = null;

    public function __construct()
    {
        if (!isset(self::$tbdb)) {
            $this->setDbObject();
            $this->createDatabase();
            $this->createTable();
        }
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function createDbObject($dbname = '')
    {
        if (isset($dbname)) {
            $host = $this->host.';dbname='.$dbname;
        }
        $tbdb = new \PDO($host, $this->dbusername, $this->dbpassword);
        $tbdb->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        return $tbdb;
    }

    public function setDbObject()
    {
        self::$tbdb = $this->createDbObject();
    }

    public function getDbObject()
    {
        return self::$tbdb;
    }

    public function createDatabase($dbname = [])
    {
        if (empty($dbname)) {
            $dbname = $this->dbname;
        }
        self::$tbdb->query("CREATE DATABASE IF NOT EXISTS $dbname");
        self::$tbdb->query("use $dbname");
    }


    public function createTable($tablesArray = [])
    {
        global $defaultTablesData;

        if (empty($tablesArray)) {
            $tablesArray = $defaultTablesData;
        }

        foreach ($tablesArray as $tableName => $query) {
            $sql = 'CREATE TABLE IF NOT EXISTS '.$tableName .'('. $query .') ENGINE=InnoDB DEFAULT CHARSET=utf8;';
//            var_dump($sql);

            self::$tbdb->query($sql);
        }
    }
}

new initDb();