<?php
require_once 'db/dbInit.php';
require_once 'config/config.php';
session_start();
$obj = initDb::getInstance();
$tbdb = $obj->createDbObject(TB_DATABASE_NAME);


class TBHelper {
    static function emailExist($email) {
        global  $tbdb;

        $stmt = $tbdb->prepare('SELECT * FROM Users WHERE email =:email');
        $stmt->execute(['email' => $email]);
        $user = $stmt->fetch(\PDO::FETCH_ASSOC);

        if ($user) {
            return $user;
        }

        return false;
    }

    static function generateToken($email, $password)
    {
        $date = date("Y.m.d");
        $salt = md5('31052019');
        $secret = md5($email.$password.$date);
        $token = $salt.md5($salt . $secret);

        return $token;
    }

    static function insertUser($userData)
    {
        global $tbdb;

        $email = $userData['tb-mail'];
        $password = $userData['tb-password'];
        $name = $userData['tb-name'];
        $token = '';

        $token = self::generateToken($email, $password);

        $sql = "
            INSERT INTO Users 
                (name, email, password, token)
            Values (?, ?, ?, ?)
        ";

        $stmt = $tbdb->prepare($sql);
        $success = $stmt->execute([ $name, $email, $password, $token]);

        if ($success) {
            $_SESSION['token'] = $token;
        }

        return $success;
    }

    static function getUserByToken($token) {
        global  $tbdb;

        $stmt = $tbdb->prepare("SELECT * FROM Users WHERE token = ?");
        $stmt->execute([$token]);
        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        return $user;
    }

    static function stringToArray($string) {
        $string = str_replace(' ', '', $string);
        $string = explode(',', $string);
        $string= array_filter($string);

        return $string;
    }
}









