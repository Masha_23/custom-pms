<?php

if (!defined('TB_PROJET_FILE_NAME')) {
    define('TB_PROJET_FILE_NAME', dirname(dirname(__FILE__)).'/');
}
require_once(TB_PROJET_FILE_NAME.'/com/config/config.php');
require_once TB_PROJET_DB_PATH.'dbInit.php';
require_once TB_PROJET_COM_PATH.'user.php';
require_once  TB_PROJET_COM_PATH.'helper.php';
require_once  TB_PROJET_COM_PATH.'tasks.php';
require_once  TB_PROJET_COM_PATH.'boards.php';


