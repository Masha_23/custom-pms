<?php
require_once 'helper.php';
require_once 'db/dbInit.php';

class TBUser {
    private $email;
    private $password;
    private $name;
    protected static $instance = null;
    private static $boards = [];

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getName() {
        return $this->name;
    }

    public function login($postData) {
        global $tbdb;
        $user = TBHelper::emailExist($postData['tb-mail']);

        if ($user) {
            if ($user['password'] == $postData['tb-password']) {
                $token = TBHelper::generateToken($user['email'], $user['password']);
                $sql = "UPDATE users SET token = ? WHERE email = ?";
                $stmt= $tbdb->prepare($sql);
                $stmt->execute([$token, $postData['tb-mail']]);
                $_SESSION['token'] = $token;
            }
        }

        return $stmt;
    }

    public function register($postData) {
        if (TBHelper::emailExist($postData['tb-mail'])) {
            return 'email exists';
        }

        $success = TBHelper::insertUser($postData);

        return $success;
    }

    public function logout() {
        unset($_SESSION['token']);
    }
}