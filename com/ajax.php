<?php
require_once 'user.php';
require_once 'tasks.php';
require_once 'boards.php';
$user = TBUser::getInstance();

if (isset($_POST['action'])) {
    $action = $_POST['action'];
    $postData = @$_POST['data']['submittedData'];

    if ($action == 'registration') {
        print $user->register($postData);
    }

    else if ($action == 'login') {
        print $user->login($postData);
    }

    else if ($action == 'logout') {
        $user->logout();
    }

    else if ($action == 'create-task') {
        $task = new TBTasks($postData);
        print $task->saveTaskData();
    }

    else if ($action == 'update-task') {
        print TBTasks::upsateTaskData($postData);
    }

    else if ($action == 'show-task') {
        print json_encode(TBTasks::getSavedTaskData($postData['taskId'], false));
    }

    else if ($action == 'create-board') {
        $board = new TBBoards($postData);
        print $board->saveBoardData();
    }
}


